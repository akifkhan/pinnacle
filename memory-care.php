   
<?php 
	include "site/header.php"; 
	$templateLead = "ViewContent"
 ?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                
                <h2 class="pb20">Memory Care</h2>
                <p>A diagnosis of Alzheimer&rsquo;s may evoke several questions. Pinnacle Senior Living is available to assist with providing answers. For those living with 
				Alzheimer&rsquo;s, dementia or other memory impairment, Pinnacle Senior Living offers personalized memory care in a secure, comfortable setting. We work with families to honor dignity and celebrate the unique life story of each resident.</p>

				<p>Alzheimer&rsquo;s Association. This unique training program provides our caring staff the specialized knowledge and skills to deliver a higher quality of dementia care.</p>

				<p>The staff of Pinnacle Senior Living receive training and consultation from the Pinnacle Senior Living Superior Memory Care Program Features:</p>
				
				<ul style="list-style: none;" align="left";>
				<li><img src="./images/arrow-bulletpoint.png" /> Intimate, Restaurant Style Dining</li>

				<li><img src="./images/arrow-bulletpoint.png" /> Structured Routines</li>

				<li><img src="./images/arrow-bulletpoint.png" /> Memory Boxes Outside of Each Residence</li>

				<li><img src="./images/arrow-bulletpoint.png" /> Superior Common Living Area</li>

				<li><img src="./images/arrow-bulletpoint.png" /> Secured Entry / Exit with Battery Back-Up</li>

				<li><img src="./images/arrow-bulletpoint.png" /> Structured Weekly Outings</li>

				<li><img src="./images/arrow-bulletpoint.png" /> Documented Routine Night Checks</li>
				</ul>
				<p>Should a hospital visit be necessary, a Pinnacle Senior Living caregiver will accompany the resident to the hospital. This caregiver will provide comfort to the resident and supply needed information to medical personnel until a family member arrives. Residents with memory limitations need an advocate to ensure their specialized needs are being met. Our Registered Nurse will follow the resident throughout their stay in the hospital and provide support to the family as needed.</p>
                               
    		</div><!-- closing row -->
			<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
