   
<?php 
	include "site/header.php"; 
	$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                <article class="col-sm-7" style="padding: 0px 0px 30px 0px">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/cQWv7XZWej4?rel=0" frameborder="0" allowfullscreen></iframe>
                    </article>
                <h2 >Assisted living can help</h2>
				<p class="pb20">Retirement is changing, and so are your options. As we age focus shifts more to safety and health, we want to continue living in our homes, however, it is not always the best option. Assisted Living can help. Pinnacle Senior Living is a beautifully designed community resting on a unique parcel of land surrounded by wooded forest providing assisted living and memory care for those with the highest of standards. Pinnacle Senior Living is where you belong. Call today to find your new home!</p> 
				<p>Subscribe to our <a href=https://www.youtube.com/channel/UC_lgtGQdJX5SQ3NMO0Ju83A> Youtube Channel</a> stay tuned for more updates on the development of Pinnacle Senior Living or go to our <a href=https://www.pinnacleseniorliving.com/contact-us.php>Contact Us</a> page to get more information and get in contact with our staff.</p>
				<div style="padding: 0px 0px 0px 26px";>
				
				</div>
                
                <div class="clearfix"></div>            
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
