   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow-down.png" /></span>
						</div><!--- closing topline --->
						
    					
    				               
                <div class="clearfix"></div>
                
                <h3>BRAND PROMISE</h3>

<p class="pb40">Pinnacle Senior Living offers a unique experience for visitors, residents, and families. Our people are friendly, professional and hard working. Our environment attracts strong employees to care for the physical, social, psychological and spiritual needs of those we serve. When it comes to the very best senior health services available, Pinnacle Senior Living is setting a superior standard!</p>

<h3>VISION</h3>

<p class="pb40">We aspire to be the leader in our industry through our superior quality of care, our modern amenities and the positive impact we have on our community and everyone we touch. We want everyone from the executive to the delivery drivers to be proud of their association with Pinnacle Senior Living. We are determined to reach this vision in a manner consistent with high ethics.</p>

<h3>MISSION</h3>

<p class="pb40">It is our mission to provide seniors a superior lifestyle that includes expert care, at an affordable option, that does not compromise quality.</p>

<h3>CORE VALUES &amp; GUIDING PRINCIPLES</h3>
<ul style="list-style: none;" align="left";>
<li><img src="./images/arrow-bulletpoint.png" /> Act with integrity, always</li>

<li><img src="./images/arrow-bulletpoint.png" /> Have passion for what you do</li>

<li><img src="./images/arrow-bulletpoint.png" /> Treat the business as your own</li>

<li><img src="./images/arrow-bulletpoint.png" /> Always exceed expectations</li>
</ul>
                    
					<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
						
                <div class="clearfix"></div>
               
                
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
