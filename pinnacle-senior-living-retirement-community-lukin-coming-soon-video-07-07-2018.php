   
<?php 
	include "site/header.php";
	$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                <article class="col-sm-7">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/8KJSm0oLX44?rel=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </article>
                <h2 >Great progress!</h2>
				<p class="pb20">You can see how much progress has been done on the construction of our new facilities! We can't wait to open our doors the you. Don't forget to make your reservations with us! Call <a href="tel:9362191165">(936) 219-1165</a> and speak to our Executive Director, <a href="management-team.php">Becky Eldridge-Clark</a> to get answers to all of your questions.</p>
				You can also submit your questions online at our <a href="contact-us.php">Contact Us</a> page.</p> 
				<p>Subscribe to our <a href="https://www.youtube.com/channel/UC_lgtGQdJX5SQ3NMO0Ju83A"> Youtube Channel</a> to stay tuned for more updates on the development of Pinnacle Senior Living.</p>
				<div style="padding: 0px 0px 0px 26px";>
				
				</div>
                
                <div class="clearfix"></div>            
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
