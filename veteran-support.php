   
<?php 
	include "site/header.php"; 
	$templateLead="Lead";
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                <article class="col-sm-6">
                     <a href="#"><img class="img-responsive" src="./images/img.png" /></a>
                    </article>
                <h2 >Veteran Support</h2>
				<h3>Attention Veterans:</h3>
                <p class="pb20">You may qualify for assistance to help cover the costs of assisted living. If you are a United States veteran , a surviving spouse of a veteran, or a widowed spouse of a veteran you may qualify for the Aid & Attendance Program and receive monthly benefits to help cover the costs of assisted living care. This benefit is not dependent upon service-related injuries for compensation. It allows for Veterans and surviving spouses who require the regular attendance of another person to assist in day-to-day activities to receive additional monetary compensation and care. This is a tax-free benefit that most veterans needing assistance qualify for.</p>
				<div style="padding: 0px 0px 0px 26px";>
				<h3>What is Aid & Attendance?</h3>

				<p>Benefits for veterans, surviving spouses, or widowed spouses who require the regular attendance of another person to assist in bathing, dressing, meal preparation, medication monitoring or other various activities of daily living. This benefit is available to individuals who reside in assisted living communities. You DO NOT have to require assistance with all of these. There simply needs to be adequate medical evidence that you cannot function completely on your own.</p>
<h3>What are the benefits of Aid &amp; Attendance?</h3>


<table width="700" border="1">
  <tr>
    <th style="text-align:center;" scope="col">Veteran Family Status</th>
    <th style="text-align:center;" scope="col">Basic Pension</th>
    <th style="text-align:center;" scope="col">Housebound</th>
    <th style="text-align:center;" scope="col">Aid & Attendance</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td style="text-align:center;"><strong>Benefits</strong></td>
    <td style="text-align:center;"><strong>Benefits</strong></td>
    <td style="text-align:center;"><strong>Benefits</strong></td>
  </tr>
  <tr>
    <td style="text-align:center;">Veteran with no dependents</td>
    <td style="text-align:center;">$12,868</td>
    <td style="text-align:center;">$15,725</td>
    <td style="text-align:center;">$21,446</td>
  </tr>
  <tr>
    <td style="text-align:center;">Veteran with a spouse or child</td>
    <td style="text-align:center;">$16,851</td>
    <td style="text-align:center;">$19,710</td>
    <td style="text-align:center;">$25,448</td>
  </tr>
  <tr>
    <td style="text-align:center;">Surviving spouse / death pension</td>
    <td style="text-align:center;">$8,630</td>
    <td style="text-align:center;">$10,548</td>
    <td style="text-align:center;">$13,794</td>
  </tr>
  
</table>
*Figures from 2016 Benefit Amount	 		

				<h3>Who is Eligible?</h3>

				</p>Any War Veteran with 90 days of active duty with at least one day during active War time. A surviving or widowed spouse of a War Veteran may be eligible if married at the time of death. The individual must qualify both medically and financially. Assets cannot exceed $80K; however, many things including home, vehicle, annuities, pre-paid funeral expenses and many other are not included in this number.</p>

				<h4>Active Duty War Eligible Dates:</h4>
				
				<ul style="list-style: none;" align="left";>
					<li>05/9/1916 - 11/11/1918</li> 
					<li>12/07/1941 - 12/31/1946</li> 
					<li>06/27/1950 - 01/31/1955 </li>
					<li>02/28/1961 - 08/5/1964 if you were serving within the Country of Vietnam only</li> 
					<li>08/5/1964 - 05/7/1975 8/20/1990-present</li>
				</ul>
				<h3>How Quickly Will I Receive the Benefit?</h3>

				<p>This is determined by the accuracy of the initial application. The average process takes around 6 months. However, the VA does pay retroactively from the date of application. The resident is paid directly from the VA.</p>

				<p>Pinnacle can help you learn more about the Aid & Attendance Program. In addition, we work with a local Veterans Service Officer, who can help families of eligible veterans and surviving spouses apply for benefits from The Department of Veteran Affairs. </p>
				</div>
                
                <div class="clearfix"></div>            
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
