<?php	

	include('MailChimp.php'); 
	
	if (is_ajax()) {
		if (isset($_POST["action"]) && !empty($_POST["action"])) { //Checks if action value exists
			$action = $_POST["action"];
			switch($action) { //Switch case for value of action
				case "test": sendemail(); break;
			}
		}
	}
	
	//Function to check if the request is an AJAX request
	function is_ajax() {
		return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest';
	}		
	
	function sendemail(){
		$return = $_POST;  
		
		
		$to = "pinnacle@rkvideo.tv"; 
		$from = $_REQUEST['email']; 
		$name = $_REQUEST['name'];  
		$headers = "From: $from"; 
		$subject = "You have a message sent from Pinaccle"; 
	
		$fields = array(); 
		$fields{"Name"} = "name"; 
		$fields{"Email"} = "email";
		$fields{"LastName"} = $lastname = "lastname";
		$fields{"Phone"} = "phone";
		$fields{"Address"} = "address";
		$fields{"Address2"} = "address2";
		$fields{"City"} = "city";
		$fields{"State"} = "state";
		$fields{"Country"} = "country";
		$fields{"Messsage"} = "message";
	
		$body = "Details:\n\n";
		foreach ($fields as $a => $b) {   
			$body .= sprintf("%s: %s\n\n", $a, $_REQUEST[$b]); 
		}

    	$send = mail($to, $subject, $body, $headers);
		
		$mailchimp = new MailChimp('a210ce64891b6b98d1cf7e5ecb0576c7-us14');
		$list = $mailchimp->call('lists/list', [
					"api_key"=>'a210ce64891b6b98d1cf7e5ecb0576c7-us14'
				]);
		$returnArray = array();

		if (!empty($list["data"])) {
			$params = [
				"apikey"=>'a210ce64891b6b98d1cf7e5ecb0576c7-us14',
				"id"=>'9914f8760e',
				"email"=>[
					"email"=> $from
				]
			];
			if ($name && strlen($name) > 0) {
				$params['merge_vars']['MERGE1'] = $name.($lastname && strlen($lastname) > 0 ? ' '.$lastname : '');
			}
			$addSubscriber = $mailchimp->call("/lists/subscribe", $params);
		}
		
		if ($send) {
			$return["json"] = "<div class='alert alert-success'><strong>Great, Your message has been sent</strong></div>";
			$return["result"] = "success";
			echo "<div class='alert alert-success'><strong>Great, Your message has been sent</strong></div>";
		} else {
			$return["json"] = "<div class='alert alert-danger'><strong>There is some problem</strong></div>";
			$return["result"] = "danger";
			echo "<div class='alert alert-danger'><strong>There is some problem</strong></div>";
		}
	}

?>