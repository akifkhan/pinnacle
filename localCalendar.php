   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
   <style>.event-link { white-space:normal; margin-bottom:24px; } h1 a { display:none; }</style> 
    <section class="section5">
                        <h2 class="pb20">Calendar of events</h2>
    	<div class="container">
    		<div class="row">
    		<?php /*	<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
              */ ?> 

		<div class="googleCalendar">
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd"><html><head><title>Pinnacle Senior Living</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link type="text/css" rel="stylesheet" href="//calendar.google.com/calendar/static/9eac25174257bc27112ce3b269f59ffahtmlembedcompiled_fastui.css">

</head>
<body class="view-month chrome-all" style="background-color: #fff; ">

<h1><a href="https://calendar.google.com"><img alt="Google Calendar" src="https://calendar.google.com/googlecalendar/images/calendarlogo/calendar_logo_en_41px_1x_r1.png"></a>
<span class="htmlembed-page-title">Pinnacle Senior Living</span></h1>
<div class="period-range">Nov 2021</div>
<table id="nav"><tr><td class="nav-buttons"><a href="https://calendar.google.com/calendar/htmlembed?src=pinnacle.info@pinnacleseniorliving.com&amp;dates=20211001/20211101"><img class="nav-button" src="https://calendar.google.com/googlecalendar/images/btn_prev.gif" alt="" height="17" width="29"></a>
<a href="https://calendar.google.com/calendar/htmlembed?src=pinnacle.info@pinnacleseniorliving.com&amp;dates=20211201/20220101"><img class="nav-button" src="https://calendar.google.com/googlecalendar/images/btn_next.gif" alt="" height="17" width="29"></a>
<button id="nav-today" onclick="location=&quot;https://calendar.google.com/calendar/htmlembed?src\x3dpinnacle.info@pinnacleseniorliving.com&quot;">Today</button></td>
<td id="month-tab" class="nav-tab"><div class="t1 tbg"></div>
<div class="t2 tbg"></div>
<a class="tab-link tbg" href="https://calendar.google.com/calendar/htmlembed?src=pinnacle.info@pinnacleseniorliving.com&amp;mode=MONTH" style="  ">Month</a></td>
<td id="agenda-tab" class="nav-tab"><div class="t1 tbg"></div>
<div class="t2 tbg"></div>
<a class="tab-link t0 tbg" href="https://calendar.google.com/calendar/htmlembed?src=pinnacle.info@pinnacleseniorliving.com&amp;mode=AGENDA" style="  ">Agenda</a></td></tr></table>
<div class="view-cap t1"></div>
<div class="view-cap t2"></div>
<div class="view-container-border"><div class="view-container"><table class="month-table"><col class="column-label" width="14%"> <col class="column-label" width="14%"> <col class="column-label" width="14%"> <col class="column-label" width="14%"> <col class="column-label" width="14%"> <col class="column-label" width="14%"> <col class="column-label" width="14%">
<tr><th class="column-label" style="  ">Sun</th> <th class="column-label" style="  ">Mon</th> <th class="column-label" style="  ">Tue</th> <th class="column-label" style="  ">Wed</th> <th class="column-label" style="  ">Thu</th> <th class="column-label" style="  ">Fri</th> <th class="column-label" style="  ">Sat</th></tr>
<tr><td class="date-marker date-not-month" style="">31</td> <td class="date-marker date-month" style="">1</td> <td class="date-marker date-month" style="">2</td> <td class="date-marker date-month" style="">3</td> <td class="date-marker date-month" style="">4</td> <td class="date-marker date-month" style="">5</td> <td class="date-marker date-month" style="">6</td></tr>
<tr class="grid-row"><td class="cell-empty cell-empty-below" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-last-row" /></td> <tr><td class="date-marker date-month" style="">7</td> <td class="date-marker date-month" style="">8</td> <td class="date-marker date-month" style="">9</td> <td class="date-marker date-month" style="">10</td> <td class="date-marker date-month" style="">11</td> <td class="date-marker date-month" style="">12</td> <td class="date-marker date-month" style="">13</td></tr>
<tr class="grid-row"><td class="cell-empty cell-empty-below" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-last-row" /></td> <tr><td class="date-marker date-month" style="">14</td> <td class="date-marker date-month" style="">15</td> <td class="date-marker date-month" style="">16</td> <td class="date-marker date-month date-today" style="">17</td> <td class="date-marker date-month" style="">18</td> <td class="date-marker date-month" style="">19</td> <td class="date-marker date-month" style="">20</td></tr>
<tr class="grid-row"><td class="cell-empty cell-empty-below" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row cell-today" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-last-row" /></td> <tr><td class="date-marker date-month" style="">21</td> <td class="date-marker date-month" style="">22</td> <td class="date-marker date-month" style="">23</td> <td class="date-marker date-month" style="">24</td> <td class="date-marker date-month" style="">25</td> <td class="date-marker date-month" style="">26</td> <td class="date-marker date-month" style="">27</td></tr>
<tr class="grid-row"><td class="cell-empty cell-empty-below" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-last-row" /></td> <tr><td class="date-marker date-month" style="">28</td> <td class="date-marker date-month" style="">29</td> <td class="date-marker date-month" style="">30</td> <td class="date-marker date-not-month" style="">1</td> <td class="date-marker date-not-month" style="">2</td> <td class="date-marker date-not-month" style="">3</td> <td class="date-marker date-not-month" style="">4</td></tr>
<tr class="grid-row"><td class="cell-empty cell-empty-below" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /><td class="cell-empty cell-last-row" rowspan="5" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-empty-below" /></td><tr class="grid-row"><td class="cell-empty cell-last-row" /></td></table></div></div>
<div class="view-cap t2"></div>
<div class="view-cap t1"></div>
<div id="footer"><span id="timezone">Timezone:
Coordinated Universal Time</span>
<div id="subscribe-link"><a target="_blank" href="render?cid=pinnacle.info%40pinnacleseniorliving.com">Subscribe with Google Calendar</a></div></div></body></html>	</div>
		<style>
		.googleCalendar {
		  position: relative;
		  height: 0;
		  margin: 0 auto;
		  padding-bottom: 100% ;
		}

		.googleCalendar iframe {
		  position: absolute; 
		  overflow: scroll;
		  width: 100%;
		  height: 100%;
		}
/* 
		@media only screen and (min-width: 800px) {
		  .googleCalendar iframe {
		    transform: scale(1.5);
		    top: 10%;
		    left: 14%;
		    width: 70%;
		    height: 60%; */
		  }
		}

		/* .googleCalendar {
		  position: relative;
		  height: 0;
		  width: 100%;
		  padding-bottom: 100% ;
		}

		.googleCalendar iframe {
		  position: absolute;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		}
		*/
	
		</style>
              
    		</div><!-- closing row -->
			<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    	</div>
    </section>
<script>
$('iframe').contents().find('.logo-plus-button-lockup').hide();
</script>
<style>
  iframe {
   /* width: 1px;
    min-width: 100%;*/
  }
</style>
<script>
  iFrameResize({ log: true }, '#calendarFrame')
</script>
<script src="localCalendar.js"></script>
<?php include "site/footer.php"; ?>
