   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                
                <article class="col-sm-6">
                     <a href="#"><img class="img-responsive" src="./images/levels-of-care.png" /></a>
                    </article>
                <h2 class="pb20">Levels of Care</h2>
                <p>At Pinnacle Senior Living we offer levels of care to ensure that residents receive individualized assistance. Our Registered Nurse will determine the appropriate level of care for each resident by completing a comprehensive assessment of each resident&rsquo;s individual abilities and needs.</p>

				<p>Within each care level, we customize care to help each resident maintain his or her highest level of independence and ability. Because no two residents are the same, the following descriptions provide only general guidelines.</p>
                               
    		</div><!-- closing row -->
			<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
						
    					<div class="row">
    						<article class="col-sm-4-2">
								<div style="height:276px;" class="post-hold">
                            	<h2>Superior Supportive Living</h2>
                                <p>Includes all Assisted Living services as well as:</p>
								<p>
									<ul style="list-style: none;" align="left";>
										<li><img src="./images/arrow-bulletpoint.png" />  Activity and mealtime reminders</li>
										<li><img src="./images/arrow-bulletpoint.png" />  Daily dressing and grooming reminders</li>
										<li><img src="./images/arrow-bulletpoint.png" />  Assistance in application of devices (dentures, hearing    aides, etc)</li> 
									</ul>
								</p>	
								</div>
                            </article><!--- closing post-hold --->
							
    						<article class="col-sm-4-2">
							<div class="post-hold">
                            	<h2>Superior Enhanced Living</h2>
                                <p>Includes all Supportive Living services as well as:</p>
								<ul style="list-style: none;" align="left";>
								<li><img src="./images/arrow-bulletpoint.png" /> Scheduled assistance with 2 weekly showers</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Occasional cognitive redirection</li>
								<li></li>
								<p>&nbsp
								&nbsp </p>
								<p>&nbsp </p>
								</ul>									
								</div>
							</article><!--- closing post-hold --->
							
    						<article class="col-sm-4-2">
							<div style="height: 287px;"class="post-hold">
                            	<h2>Superior Comprehensive Living</h2>
                                <p>Includes all Supportive Living services as well as:</p>
								<ul style="list-style: none;" align="left";>
								<li><img src="./images/arrow-bulletpoint.png" /> Escort to and from dining</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Occasional behavioral redirection</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Scheduled assistance with 3 or more showers</li>
								</ul>
								</div>
							</article><!--- closing post-hold --->
							
							<article class="col-sm-4-2">
							<div class="post-hold">
                            	<h2>Superior Extensive Living</h2>
                                <p>Includes all Supportive Living services as well as:</p>
								<ul style="list-style: none;" align="left";>
								<li><img src="./images/arrow-bulletpoint.png" /> Management of Incontinence</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Full assistance with dressing and grooming</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Additional laundry/housekeeping</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Support with transfers, involving the assistance of one person</li>
								</ul>
								</div>
							</article><!-- closing row -->
    					</div> 
    	</div>
    </section>

<?php include "site/footer.php"; ?>
