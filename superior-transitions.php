   
<?php include "site/header.php"; ?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
					<div class="clearfix"></div>
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
					
                <h2 class="pb20">Superior Transitions</h2>
                <p>At Pinnacle, we understand that relocating is one of life&rsquo;s most difficult experiences. Seniors have lived in their home for a lifetime. They&rsquo;ve raised families and are surrounded by special memories, precious heirlooms, and family history. When a senior moves they downsize. They don&rsquo;t take everything with them. Superior Transitions can provide expertise and an objective outlook to make your move a positive experience for both the resident and family members. </p>

				<p class="pb40"> For an all-inclusive rate, Pinnacle will assist you with the move, through our Superior Transitions service. Imagine walking into your new apartment with furniture in place, clothes in the closet, phone and TV connected, bed made and pictures hung. That is what you receive through our Superior Transitions service. At the end of the day, it won&rsquo;t look like moving day; it will look like you&rsquo;ve come home!</p>
                						
    					<div class="row">
    						<article class="col-sm-4">
								<div style="height:319px;" class="post-hold">
                            	<h2>Preparation for the move</h2>
                                <div class="topline-2 text-center">
								</div><!--- closing topline --->
                                <p class="">
									<ul style="list-style: none;" align="left";>
										<li><img src="./images/arrow-bulletpoint.png" /> Home visit to select items (furniture, pictures, dishes, etc.)</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Floor planning of the furniture placement</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Moving totes provided</li>	
									</ul>									
								</p>
								</div>
								
                            </article><!--- closing post-hold --->
							
    						<article class="col-sm-4">
							<div style="height:319px;" class="post-hold">
                            	<h2>Coordination of the move</h2>
                                <div class="topline-2 text-center">
								</div><!--- closing topline --->
                                <p>
									<ul style="list-style: none;" align="left";>
										<li><img src="./images/arrow-bulletpoint.png" /> Moving date scheduled through a professional, insured moving company</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Family and Resident made aware of moving date and time</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Floor plan reviewed</li>
									</ul>								
								</p>
								</div>
                            <!--- closing post-hold --->
							</article>
							
    						<article class="col-sm-4">
							<div class="post-hold">
                            	<h2>Preparation of your new apartment</h2> 
                                <div class="topline-2 text-center">
								</div><!--- closing topline --->
                                <p class="">
									<ul style="list-style: none;" align="left";>
										<li><img src="./images/arrow-bulletpoint.png" /> Complete assembly of kitchen, bedroom and bath</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Hanging of pictures, drapes and shower curtain</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Furniture Placement</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Removal of Moving Supplies</li>
										<li><img src="./images/arrow-bulletpoint.png" /> Ensure utilities working properly</li>
									</ul>
								</p>
							</div>
                        </div>
							</article>	
    					</div> <!-- closing row -->
    				
    			</div>
                
                <div class="clearfix"></div>
                <img style="display:block; margin-left:auto; margin-right:40px;" alt="pet-friendly-logo" src="./images/pet-friendly-crest.png" width="70" height="101"align="right">
                <h3>Transition Tips</h3>

				<strong>Be Patient</strong>

				<p>Adjustment can take anywhere from 30 to 90 days.</p>

				<strong>Keep an open mind</strong>

				<p>This is absolutely imperative. Since the move into assisted living requires change and adjustment, loved ones are more likely to adapt well if they understand and prepare for this.</p>

				<strong>Ask questions</strong>

				<p>Encourage your loved one to always ask the staff any questions they might have about their new home.</p>

				<strong>Bring personal items</strong>

				<p>Surround your loved one with familiar items that make their new environment feel like home.</p>
                
                <div class="clearfix"></div>
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
                
                
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
