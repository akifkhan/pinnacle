   
<?php include "site/header.php"; ?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow-down.png" /></span>
						</div><!--- closing topline --->
						
    					
    				               
                <div class="clearfix"></div>
                
                <h2 style="color:#172741" class="pb20">What Makes Us Different</h2>
				
                <p class="pb40">It is our mission to provide seniors with a superior lifestyle that includes expert care at an affordable option that does not compromise quality.</p>
                    
					<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
						
                <div class="clearfix"></div>
               
                
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
