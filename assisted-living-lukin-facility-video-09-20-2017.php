   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                <article class="col-sm-7">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/35hnxBheLOs?rel=0" frameborder="0" allowfullscreen></iframe>
                    </article>
                <h2 >Construction Progress</h2>
				<p class="">Here is a flyover update of the construction progress of the Pinnacle Senior Living facilities. Remember, you can call us to get answers for all of your questions about your future apartment and community. Reserve now, Move later! Call <a href="tel:9362191165">(936) 219-1165</a> and speak to our Executive Director, <a href="management-team.php">Becky Eldridge-Clark</a>.</p>
				You can also submit your questions online at our <a href="contact-us.php">Contact Us</a> page, and even message us on our Facebook page <a href="https://www.facebook.com/lufkinseniorliving/">Pinnacle Senior Living</a>.</p> 
				<p>Subscribe to our <a href="https://www.youtube.com/channel/UC_lgtGQdJX5SQ3NMO0Ju83A"> Youtube Channel</a> to stay tuned for more updates on the development of Pinnacle Senior Living.</p>
				<div style="padding: 0px 0px 0px 26px";>
				
				</div>
                
                <div class="clearfix"></div>            
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
