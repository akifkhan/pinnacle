   
<?php 
	include "site/header.php"; 
	$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                <article class="col-sm-7">
                     <iframe width="560" height="315" src="https://www.youtube.com/embed/U_n1YXqbclA?rel=0" frameborder="0" allowfullscreen></iframe>
                    </article>
                <h2 >Coming Soon!</h2>
				<p class="pb20">Construction is underway at 614 W. Whitehouse Drive in  Lufkin Texas! Soon you will be able to enjoy superior quality of care in our beautifully designed community resting on a unique parcel of land with wooded forest space. Check this aerial footage of the contrusction site in progress.</p> 
				<p>Subscribe to our <a href=https://www.youtube.com/channel/UC_lgtGQdJX5SQ3NMO0Ju83A> Youtube Channel</a> stay tuned for more updates on the development of Pinnacle Senior Living or go to our <a href=https://www.pinnacleseniorliving.com/contact-us.php>Contact Us</a> page to get more information and get in contact with our staff.</p>
				<div style="padding: 0px 0px 0px 26px";>
				
				</div>
                
                <div class="clearfix"></div>            
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
