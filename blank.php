   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow-down.png" /></span>
						</div><!--- closing topline --->
						
    					
    				               
                <div class="clearfix"></div>
                


<div class="pb40"><div>We aspire to be the leader in our industry through our superior quality of care, our modern amenities and the positive impact we have on our community and everyone we touch. We want everyone from the executive to the delivery drivers to be proud of their association with Pinnacle Senior Living. We are determined to reach this vision in a manner consistent with high ethics.</div></div>


                <div class="clearfix"></div>
                    
		<div class="topline text-center">
		<span><img src="./images/arrow.png" /></span>
		</div><!--- closing topline --->
						
               
                
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
