   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
						
    					<div class="row">
    						<article class="col-sm-4">
								<div class="post-hold">
                            	<h2>Superior Lifestyle</h2>
                                <p>
									<ul style="list-style: none;" align="left";>
								<li><img src="./images/arrow-bulletpoint.png" />  Gourmet Meals</li>
								<li><img src="./images/arrow-bulletpoint.png" />  Scheduled Transportation</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Waterfront Outdoor Terrace</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Resident Enrichment Director</li>
									</ul>	
								</p>
								<p>&nbsp;</p>
								<p>&nbsp;</p>
								&nbsp;
								</div>
                            </article><!--- closing post-hold --->
							
    						<article class="col-sm-4">
							<div class="post-hold	">
                            	<h2>Superior Apartment Home</h2>
                                <p>
									<ul style="list-style: none; padding:0px 0px 0px 26px"; align="left";>
								<li><img src="./images/arrow-bulletpoint.png" /> All standard Utilities Included</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Weekly Housekeeping/Laundry Service</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Daily Trash Removal</li>
								<li><img src="./images/arrow-bulletpoint.png" /> 9' or 10' Ceilings</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Large Kitchenette</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Window Coverings</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Pre-wiring for High-speed Internet</li>
									</ul>
								</p>
                            </div><!--- closing post-hold --->
							</article>
							
    						<article class="col-sm-4">
							<div class="post-hold">
                            	<h2>Superior Living</h2>
                                <p>
									<ul style="list-style: none; padding:0px 0px 0px 26px"; align="left";>
								<li><img src="./images/arrow-bulletpoint.png" /> All standard Utilities Included</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Weekly Housekeeping/Laundry Service</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Daily Trash Removal</li>
								<li><img src="./images/arrow-bulletpoint.png" /> 9' or 10' Ceilings</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Large Kitchenette</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Window Coverings</li>
								<li><img src="./images/arrow-bulletpoint.png" /> Pre-wiring for High-speed Internet</li>
									</ul>
								</p>
                            </div>
							</article>
						</div> 
							<div class="row">
								<img style="display:block; margin:auto;" alt="pet-friendly-logo" src="./images/pet-friendly-crest.png" width="118" height="170"align="middle">
							</div>
    					<!-- closing row -->
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                
                <div class="clearfix"></div>
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
