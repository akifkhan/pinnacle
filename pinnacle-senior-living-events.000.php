   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
                        <h2 class="pb20">Calendar of events</h2>
    	<div class="container">
    		<div class="row">
    		<?php /*	<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
              */ ?> 

		<div class="googleCalendar">
		<!--  iframe src="https://calendar.google.com/calendar/embed?src=pinnacle.info%40pinnacleseniorliving.com&ctz=America/Chicago" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe -->
			<!-- iframe src="https://calendar.google.com/calendar/embed?title=&nbsp;&amp;showCalendars=0&amp;height=800&amp;wkst=1&amp;bgcolor=%23C4D7FD&amp;src=pinnacle.info%40pinnacleseniorliving.com&amp;ctz=America/Chicago&amp;color=%23C4D7FD&amp;" style="border-width:0" width="800" height="800" frameborder="0" scrolling="no"></iframe-->	
			<!-- iframe id="calendarFrame" src="https://calendar.google.com/calendar/embed?title=&nbsp;&amp;showCalendars=0&amp;height=1000&amp;wkst=1&amp;bgcolor=%23C4D7FD&amp;src=pinnacle.info%40pinnacleseniorliving.com&amp;ctz=America/Chicago&amp;color=%23C4D7FD&amp;" style="border-width:0" width="800" height="1000" frameborder="0" scrolling="no"></iframe -->
				<iframe id="calendarFrame" src="localCalendar.php" style="border-width:0" width="800" height="1500" frameborder="0" scrolling="no"></iframe>
			<script>
			    var iframe = document.getElementById("calendarFrame");
                // var elmnt = iframe.contentWindow.document.getElementsByTagName("H1")[0];
                // elmnt.style.display = "none"; 
                var eventItems = iframe.contentWindow.document.querySelectorAll(".te-s");

                eventItems.forEach(function(eventItem) {
                  remove(eventItem);
                });
			</script>
		</div>
		<style>
		.googleCalendar {
		  position: relative;
		  height: 0;
		  margin: 0 auto;
		  padding-bottom: 100% ;
		}

		.googleCalendar iframe {
		  position: absolute; 
		  overflow: scroll;
		  width: 100%;
		  height: 100%;
		}
/* 
		@media only screen and (min-width: 800px) {
		  .googleCalendar iframe {
		    transform: scale(1.5);
		    top: 10%;
		    left: 14%;
		    width: 70%;
		    height: 60%; */
		  }
		}

		/* .googleCalendar {
		  position: relative;
		  height: 0;
		  width: 100%;
		  padding-bottom: 100% ;
		}

		.googleCalendar iframe {
		  position: absolute;
		  top: 0;
		  left: 0;
		  width: 100%;
		  height: 100%;
		}
		*/
	
		</style>
              
    		</div><!-- closing row -->
			<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    	</div>
    </section>
<script>
$('iframe').contents().find('.logo-plus-button-lockup').hide();
</script>
<style>
  iframe {
   /* width: 1px;
    min-width: 100%;*/
  }
</style>
<script>
  iFrameResize({ log: true }, '#calendarFrame')
</script>
<?php include "site/footer.php"; ?>
