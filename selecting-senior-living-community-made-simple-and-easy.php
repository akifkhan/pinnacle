   
<?php 
	include "site/header.php"; 
	$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
    			</div>
                
                <div class="clearfix"></div>
                <article class="col-sm-7" style="padding: 0px 0px 71px 0px">
                   <iframe width="560" height="315" src="https://www.youtube.com/embed/lfRmNU0td8U" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                    </article>
                <h2 >Selecting a Senior Living Community made simple and easy!</h2>
				<p class="pb20">Aging brings a unique set of challenges, and selecting a senior living community should NOT be one of them.
				Pinnacle Senior Living we understands these challenges and promises to make the selection process simple and easy. We offer a month to month agreement with NO large entrance fee and NO Large buy-in requirements. At Pinnacle Senior Living your happiness is our agreement. Call today at <a href="tel:9362191165">(936) 219-1165</a> to make a refundable deposit and begin your move-in process.</p> 
				<p>Subscribe to our <a href=https://www.youtube.com/channel/UC_lgtGQdJX5SQ3NMO0Ju83A> Youtube Channel</a> stay tuned for more updates on the development of Pinnacle Senior Living or go to our <a href=https://www.pinnacleseniorliving.com/contact-us.php>Contact Us</a> page to get more information and get in contact with our staff.</p>
				<div style="padding: 0px 0px 0px 26px";>
				
				</div>
                
                <div class="clearfix"></div>            
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
