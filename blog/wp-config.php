<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pinnacle_wp');

/** MySQL database username */
define('DB_USER', 'pinnacle_user');

/** MySQL database password */
define('DB_PASSWORD', 'Superm@n1');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'd7jyL~RzGrj0CY/H6xcy@blFv+7Cu0/;YujU*yqlgFQx^UQ{>CyU6c-X}z,!XS2u');
define('SECURE_AUTH_KEY',  '{m#b<2Y(IQ-Kx(Q62jz4W`Au8Z*Mm[#^Tzam?YB3X/f9ohylm-Fav)?s7z5w^80P');
define('LOGGED_IN_KEY',    '.`wuuUr}.D_C94.L0rXc#41!b>JfK3Rl?k&i&E-j25qh.qq^9vnwzGvK<i%SQgAu');
define('NONCE_KEY',        'l($u$q:(O)QLWA;hI9(fk]fr5E9in05%GcQAtfN_/h~dW>=tZ-ozNNxXriJ~Qi}{');
define('AUTH_SALT',        'kh<;~GM!C+iseF#BbHJ5k-qU NS.1f@GqEy}q.mlQ|XtY>Fo=Jnh:3eY)gRs@q?v');
define('SECURE_AUTH_SALT', '^*K|Gvxl1j{&c!P&F,KCa([yE7Ils(Zqx(>f_]g=i$DFk/<%r%~Il-}.qKHy2(-R');
define('LOGGED_IN_SALT',   '#=nc%~yk-YnkU#+A+f!l.iUW@#^}#N,coCchO!M?>{Y(foFi(NL`N$-C75RrO3DA');
define('NONCE_SALT',       'zsfBjvynymX2%[D;g*oEnmV/e6KKjo}N) :BhyPVYFQ:EU|1voS-gk0~lpL?iU;/');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'press_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
