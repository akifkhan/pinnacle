
  <article id="post-0" class="post not-found">
    <div class="header">
      <h1 class="entry-title"><?php _e( 'Not Found', 'supersimple' ); ?></h1>
    </div>
    <section class="entry-content">
      <p><?php _e( 'Nothing found for the requested page. Try a search instead?', 'supersimple' ); ?></p>
      <?php get_search_form(); ?>
    </section>
  </article>
