   
<?php include "site/header.php"; ?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
						
    			</div>
                
                <div class="clearfix"></div>
                
                <h2 class="pb20">Respite Stay</h2>
                <p>Pinnacle Senior Living offers short-term, overnight stays for individuals needing Memory Care arrangements. A respite stay is a great option should your loved one need a temporary place to live while a caregiver enjoys a well-deserved break. Our overnight respite program provides a safe and stimulating atmosphere. A minimum of seven nights and prequalification will be required for a respite stay.</p>
				<p>&nbsp;
								&nbsp; </p>
				<p>&nbsp;
								&nbsp; </p>
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
						</div><!--- closing topline --->
						
                <div class="clearfix"></div>
                
                <h2 class="heading-hold">Superior Assisted Living & Memory Care</h2>
                
                <div class="row mr0 ml0 mt70 mb30 single-post">
                	<article class="col-sm-6">
                     <a href="#"><img class="img-responsive" src="./images/img.png" /></a>
                    </article>
                	<article class="col-sm-6">
                    	<p>You may qualify for assistance to help cover the costs of assisted living. If you are a United States veteran , a surviving spouse of a veteran, or a widowed spouse of a veteran you may qualify for the Aid & Attendance Program and receive monthly benefits to help cover the costs of assisted living care... <a class="link-hold" href="veteran-support.php">Read More</a></p>
                    </article>
                </div>
                
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
