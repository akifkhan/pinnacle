<?php
$templateTitle="Pinnacle Senior Living";
$templateHeading="Pinnacle Senior Living, Superior Senior Care in Lufking TX";
$templateDescription="Pinnacle Senior Living is a beautifully designed community resting on a unique parcel of land with wooded forest space, providing Assisted Living and Memory Care for those with the highest of standards";
$templateLead = "ViewContent";
$templateCss = <<<ENDCSS

ENDCSS;
$templateScript = <<<ENDSCRIPT

ENDSCRIPT;
// $template="";
?>   
<?php include "site/header.php"; ?>
    <div class="container slider-hold">
    	<div class="row">
    		<div class="col-sm-12">
					<div data-ride="carousel" class="carousel slide" id="myCarousel">
					  <ol class="carousel-indicators">
						<li class="active" data-slide-to="0" data-target="#myCarousel"></li>
						<li data-slide-to="1" data-target="#myCarousel" class="first-slide"></li>
						<li data-slide-to="2" data-target="#myCarousel" class="second-slide"></li>
						<li data-slide-to="3" data-target="#myCarousel" class="third-slide"></li>
						<li data-slide-to="4" data-target="#myCarousel" class="fourth-slide"></li>
						<li data-slide-to="5" data-target="#myCarousel" class="fifth-slide"></li>
						<li data-slide-to="6" data-target="#myCarousel" class="sixth-slide"></li>
						<li data-slide-to="7" data-target="#myCarousel" class="seventh-slide"></li>
					  </ol>
					  <div role="listbox" class="carousel-inner">
					      <div class="item text-center">
						  <img alt="Seventh slide" src="./images/banner-slide-6.png" class="seventh-slide">
						</div>
						<div class="item text-center">
						  <a href="pinnacle-senior-assisted-living-can-help.php"><img alt="First slide" src="./images/banner-slides-5.png" class="fisrt-slide"></a>
						</div>
						<div class="item text-center active">
						  <img alt="Second slide" src="./images/banner-slide-1.png" class="second-slide">
						</div>
						<div class="item text-center">
						  <img alt="Third slide" src="./images/banner-slide-2.png" class="third-slide">
						</div>
						<div class="item text-center">
						  <img alt="Fourth slide" src="./images/banner-slide-3.png" class="fourth-slide">
						</div>
						<div class="item text-center">
						  <img alt="Fifth slide" src="./images/banner-slide-4.png" class="fifth-slide">
						</div>
						<div class="item text-center">
						  <img alt="Sixth slide" src="./images/banner-slide-5.png" class="sixth-slide">
						</div>
					</div>
					 <!---  <a data-slide="prev" role="button" href="#myCarousel" class="left carousel-control">
						<span aria-hidden="true" class="glyphicon glyphicon-chevron-left"></span>
					  </a>
					  <a data-slide="next" role="button" href="#myCarousel" class="right carousel-control">
						<span aria-hidden="true" class="glyphicon glyphicon-chevron-right"></span>
					  </a> --->
					  
					  
					</div>
    		</div>
    	</div>
    </div>
	<!--- end slider --->
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow-down.png" /></span>
						</div><!--- closing topline --->
	                
                <div class="clearfix"></div>
                
                <h2 class="pb20">Welcome to Pinnacle Senior Living </h2>
                <p>Pinnacle Senior Living is a beautifully designed community resting on a unique parcel of land with wooded forest space, providing Assisted Living and Memory Care for those with the highest of standards.  Our highly qualified staff will ensure you receive the pampering and attentive care you deserve, including 24-hour support when you need it.  Whether enjoying a spectacular meal on our waterfront terrace or participating in one of the many social events, you will quickly find that Pinnacle Senior Living provides you with a superior combination of elegance and comfort.</p>  

				<p>It is the perfect time to consider a new start in a beautiful, maintenance-free apartment home at Pinnacle Senior Living.  We have an amazing variety of stylish one and two-bedroom floor plans to choose from.  With so many extraordinary benefits, it is no surprise that Pinnacle Senior Living apartments are in high demand.</p>

				<p class="pb40">When it comes to the very best senior health services available in Lufkin, Pinnacle Senior Living is setting a superior standard.  As you turn the page to a new chapter in your life, we await with open doors and a warm welcome.  We hope you will choose to become part of our family.</p>
			
                
                <div class="clearfix"></div>
                
				<div class="topline text-center">
						<span><img src="./images/arrow-down.png" /></span>
						</div><!--- closing topline --->

						<iframe width="1070" height="525" src="https://www.youtube.com/embed/cyMtsKQ8dbY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
							<br><br>
						<div class="row">
    						<article class="col-sm-4">
								<div class="post-hold">
                            	<h2>The Appropriate<br>Level of Care for your Loved One</h2>
                                <p>Enjoy comfort and elegance in Lufkin's Superior Senior living community. As you turn the page to a new chapter in your life, we await with open doors and a warm welcome. <a href="brand-promise.php">Read More...</a></p>
								</div>
                            </article><!--- closing post-hold --->
							
    						<article class="col-sm-4">
							<div class="post-hold">
                            	<h2>Conveniently and Beautifully Located</h2>
                                <p>At Pinnacle we offer levels of care to ensure that residents receive individualized assistance. Our Registered Nurse will determine the appropriate level of care...
								<a href="levels-of-care.php">Read More...</a></p>
                            </div><!--- closing post-hold --->
							</article>
							
    						<article class="col-sm-4">
							<div class="post-hold">
                            	<h2>Our Superior Senior Living Community.</h2>
                                <p>Pinnacle's Superior Assisted Living and Memory Care is beautifully and conveniently located at 614 W. Whitehouse, Lufkin Tx <br>
								<a href="contact-us.php">Read More...</a></p>
                            </div>
							</article>
    					</div> <!-- closing row -->
						<div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
    			</div>
    	</div>
	<p style="margin-bottom:48px; text-align:center;">&ldquo;<small>A long-term care ombudsman helps residents of a nursing facility and residents of an assisted living facility resolve complaints. Help provided by an ombudsman is confidential and free of charge. <strong>To speak with an ombudsman, a person may call the toll-free number <a href="tel:+18002522412">1-800-252-2412</a>.</strong></small>&rdquo;</p>
</section>

<?php include "site/footer.php"; ?>
