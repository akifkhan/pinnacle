<!doctype html>
<html>
<head>
	<meta charset="UTF-8">
	
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<title><?php echo $templateTitle; ?></title>
		
		<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
 		<link rel="stylesheet" href="css/style.css" />
		<link rel="stylesheet" href="css/media.css" />
		<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css" />

		<style>

		<?php echo $templateCss; ?>

		</style>

		<link rel="apple-touch-icon" sizes="57x57" href="/pinnacle-icons/pinnacle-icons/apple-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/pinnacle-icons/pinnacle-icons/apple-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/pinnacle-icons/apple-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/pinnacle-icons/apple-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/pinnacle-icons/apple-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/pinnacle-icons/apple-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/pinnacle-icons/apple-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/pinnacle-icons/apple-icon-152x152.png">
		<link rel="apple-touch-icon" sizes="180x180" href="/pinnacle-icons/apple-icon-180x180.png">
		<link rel="icon" type="image/png" sizes="192x192"  href="/pinnacle-icons/android-icon-192x192.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/pinnacle-icons/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="96x96" href="/pinnacle-icons/favicon-96x96.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/pinnacle-icons/favicon-16x16.png">
		<link rel="manifest" href="/pinnacle-icons/manifest.json">
		<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
		<link rel="icon" href="/favicon.ico" type="image/x-icon">
		<meta name="msapplication-TileColor" content="#ffffff">
		<meta name="msapplication-TileImage" content="/ms-icon-144x144.png">


		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	
		<script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
		<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-NLCFLDJ');</script>
<!-- End Google Tag Manager -->
   <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-90381097-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '1483225771733568');
  fbq('track', 'PageView');
</script>
<noscript><img alt="" height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=1483225771733568&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->



<?php if ($templateLead == "Lead") { ?>

<script>
  fbq('track', 'Lead');
</script>

<?php } ?>

<?php if ($templateLead == "ViewContent") { ?>

<script>
  fbq('track', 'ViewContent');
</script>

<?php } ?>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NLCFLDJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->


	<section class="section1">
		<div class="container">
			<div class="row">
				<div class="col-sm-12" style="text-align:center;">
					<p class="top-bar-outer"> <a class="top-bar-schedule" href="contact-us.php">Schedule a Tour</a> <span class="top-bar-call"> or call Us at:<a onclick="goog_report_conversion ('tel:9362191165')" href="tel:9362191165"> (936) 219-1165</a>   -</span>  <a class="top-bar-link symbol map" href="https://www.google.com/maps/dir//31.2993992,-94.7392571/@31.2987259,-94.7701783,13z/data=!3m1!4b1" target="_blank" title="Pinnacle Senior Living Google Maps"> 615 W. Whitehouse Dr, Lufkin, 75904</a></p>
				</div>
			</div>
		</div>
	</section>
    
    <section class="section2">
    	<div class="container">
    		<div class="row top-bar">
    			<div class="col-sm-1 social-hold"> 
                	<a href="https://www.youtube.com/channel/UC_lgtGQdJX5SQ3NMO0Ju83A"><img alt="" src="./images/youtube.png" /></a>
                    <a href="#"><img alt="" src="./images/instagram.png" /></a>
                    <a href="https://www.facebook.com/lufkinseniorliving/" target="_blank"><img alt="" src="./images/facebook.png" /></a>
                </div>
    			<div class="col-sm-11 logo-hold">
                	<!-- <a href="#"><img alt="" class="img-responsive" src="./images/logo.png" /></a> -->
                </div>
    		</div>
    	</div>
    </section>
    
    
    <section class="section3">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
					<nav class="navbar navbar-inverse navbar-static-top">
						<div class="navbar-header">
						  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						  </button>
						</div>
						<div id="navbar" class="navbar-collapse collapse">
						  <ul class="nav navbar-nav">
							<li class="active"><a href="//pinnacleseniorliving.com/index.php">Home</a></li>
							<li class="dropdown">
							  <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Who We are <span class="caret"></span></a>
							  <ul class="dropdown-menu">
								<li><a href="//pinnacleseniorliving.com/brand-promise.php">What makes us Different</a></li>
								<li><a href="//pinnacleseniorliving.com/management-team.php">Management Team</a></li>
								<li><a href="//pinnacleseniorliving.com/blog/">Blog</a></li>
								<li><a href="//pinnacleseniorliving.com/pinnacle-senior-living-events.php">Calendar</a></li>
							  </ul>
							</li>
							<li><a href="//pinnacleseniorliving.com/living-options.php">Living Options</a></li>
							<li><a href="//pinnacleseniorliving.com/veteran-support.php">Veteran Support</a></li>
							<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Amenities<span class="caret"></span></a>
							 <ul class="dropdown-menu">
								<li><a href="//pinnacleseniorliving.com/apartment-amenities.php">Apartment Amenities</a></li>
								<li><a href="//pinnacleseniorliving.com/superior-transitions.php">Superior Transitions</a></li>
							  </ul></li>
							<li><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Our Care<span class="caret"></span></a>
							 <ul class="dropdown-menu">
								<li><a href="//pinnacleseniorliving.com/levels-of-care.php">Levels of Care</a></li>
								<li><a href="//pinnacleseniorliving.com/memory-care.php">Memory Care</a></li>
								<li><a href="//pinnacleseniorliving.com/respite-stay.php">Respite Stay</a></li>
							  </ul></li>
							<li><a href="//pinnacleseniorliving.com/contact-us.php">Contact Us</a></li>
						  </ul>
						</div>
					</nav>    			
				</div>
    		</div>
    	</div>
    </section>
    
    
    
 
