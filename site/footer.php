    
    
    <footer class="footer">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
    				<ul class="list-unstyled">
    					<li><a class="top-bar-link" href="https://www.google.com/maps/dir//31.2993992,-94.7392571/@31.2987259,-94.7701783,13z/data=!3m1!4b1" target="_blank" title="Pinnacle Senior living Google maps" class="symbol map"> 615 W. Whitehouse Dr, Lufkin, 75904</a></li>
    					<li><a href="tel:9362191165"> (936) 219-1165</a></li>
    					<li> Facility ID 107217</li>
    					<li><a href="https://www.pinnacleseniorliving.com/sitemap">Sitemap</a></li>
    					
    				</ul>
                    <ul class="list-unstyled">
                    <li>-</li>
                    <li>Web Design by <a href="http://rkvideo.tv">RK Productions</a></li>
                    <li>-</li>
                    </ul>
    			</div>
    		</div>
    	</div>
    </footer>

    <script>(function(d,t,u,s,e){e=d.getElementsByTagName(t)[0];s=d.createElement(t);s.src=u;s.async=1;e.parentNode.insertBefore(s,e);})(document,'script','//chat.pinnacleseniorliving.com/php/app.php?widget-init.js');</script>
    <!-- Global site tag (gtag.js) - Google Ads:833926205-->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-833926205"></script>
    <script>
        window.dataLayer = window.dataLayer ||[];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'AW-833926205');
    </script>
    <script>
      function gtag_report_conversion(url) {
          var callback = function () {
            if (typeof(url) != 'undefined') {
            window.location = url;
          }
        };
      gtag('event', 'conversion', {
          'send_to': 'AW-833926205/gMOpCLqB3v0BEL3o0o0D',
          'event_callback': callback
      });
      return false;
    }
</script>
</body>
</html>
