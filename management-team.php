 <?php  
        $templateTitle = "Management Team";
        $templateCss = <<<CSS

            .img-responsive {
                max-width: 400px;
                max-height: max-content;
                padding-bottom: 36px;
            }

            article,
            .topline,
            hr {
                clear: both;
            }

            .text-column {
                margin-bottom: 36px;
            }
CSS;
        include "site/header.php";
?>
 <section class="section5">
     <div class="container">
         <div class="row">

             <div class="topline text-center"> <span><img alt="" src="./images/arrow-down.png" /></span> </div>
             <!--- closing topline --->
             <article> <img alt="Cheryl" class="img-responsive col-sm-6" src="./images/cheryl-j01.jpg" />
                 <div class="col-sm-6 text-column">
                     <h2>Cheryl Jaskiewicz, CDAL</h2>
                     <h3 class="pb20">Chief Operating Officer</h3>
                     <p>Cheryl Jaskiewicz and her husband John are residents of Longview, Texas. They have two children,
                         Tyler and Dawson, who keep them very young. Cheryl is excited to share her expertise and
                         compassion for seniors. She brings high energy into the work environment and shows strong
                         leadership skills with her team. Since 2004, she has felt a strong compassion as well as reward
                         in serving individuals with Dementia. Having been personally affected with a family member
                         having Alzheimer&rsquo;s Disease, she is able to relate and be a support to families.</p>
                     <p>Cheryl&rsquo;s 16-year professional career has been focused on the senior population; proving
                         her passion by becoming a Certified Director of Assisted Living (CDAL). This certification
                         utilizes standards for best practice in the assisted living industry, taking existing state
                         licensing requirements to the next level. While serving as a facility administrator, Cheryl won
                         the ALFA Best of the Best Award in Human Resources which was presented to her at the ALFA
                         Philadelphia conference. As a team, Cheryl set up the Ambassador program by using her senior
                         care partners and management team in the hiring, training and mentoring of new staff members.
                         Cheryl realized through her tenure in serving the elderly that consistency in training and
                         service delivery provided successful results in customer and staff satisfaction. She has also
                         received the ALFAs Champion award after being nominated by a family member for the
                         extraordinary care and compassion provided to her loved one. This award recognizes the
                         exemplary professional service and commitment to seniors. Ms. Jaskiewicz is a current member of
                         Argentum and serves on the Longview Chamber of Commerce Board of Directors.</p>
                     <p>Cheryl has a great passion to accomplish, to work outside of the box, and to work through
                         whatever challenges face the community. Cheryl will strive to make every day the best day in
                         the life of the residents that reside in her community.</p>
                     <p>When it comes to the very best senior health service available in Lufkin, Pinnacle Senior Living
                         is setting a superior standard. As you turn the page to a new chapter in your life, we await
                         with open doors and a warm welcome. We hope you will become part of our family.</p>
                 </div>
             </article>
             <hr />
             <article> <img alt="Kandes" class="img-responsive col-sm-6" src="./images/Kandes_Holbrook.jpg" />
                 <div class="col-sm-6 text-column">
                     <h2>Kandes Holbrook</h2>
                     <h3 class="pb20">Executive Director</h3>
                     <p>Kandes Holbrook and her husband Ross are residents of Nacogdoches, Texas in the Woden Community.
                         They are proud parents of a blended family of 5 children. All of which are grown adults and
                         leading lives of their own. They also have 7 grandchildren ranging in age from 20 years of age
                         to newborn. Kandes enjoys spending time with her family and friends. Kandes and her husband
                         enjoy water sports during the summer. She enjoys working in the senior community and has a
                         passion for making a difference in the lives of the residents she works with.</p>
                     <p>Kandes’ career has been focused on the senior community beginning in 1988 in long term care. Her
                         career began in Housekeeping and Laundry. Quickly, she moved up to coming a Certified Nursing
                         Assistant, specializing in assisting residents on Physical Therapy Services. As her love for
                         Seniors became evident she was given various opportunities within the community, further
                         expanding her knowledge of senior living and leaving her with invaluable experience in the
                         field. </p>
                     <p>In 2013, she received her Activity Director Certification from Angelina College. She became
                         employed at a local retirement community and in 2014 became Certified with the State of Texas
                         as an Advanced Professional Activity Director. In 2016 she became certified nationally, and in
                         2018 she received her certification as a Certified Dementia Practioner. Then in 2019, she took
                         classes from Texas Assisted Living Association to become a Assisted Living Manager.</p>
                     <p>Because of her own personal experience, Kandes has a lot of compassion for families that are
                         affected by dementia. She has the desire to help not only the residents, but also their
                         families. She strives to be a team player and will ensure the residents have the dignity and
                         respect they so deserve.</p>
                 </div>
             </article>
             <hr />
             <article> <img alt="Cyndi" class="img-responsive col-sm-6" src="./images/Cyndi_Poindexter.jpg" />
                 <div class="col-sm-6 text-column">
                     <h2>Cyndi Poindexter, LVN</h2>
                     <h3 class="pb20">Wellness Director</h3>
                     <p>Cyndi Poindexter and her husband John are residents of Huntington, Tx in the Ora community. They
                         have 1 son, Chad, and a 4 year old granddaughter, “Kenny Grace”. They also are parents to 2
                         dogs and a very social parrot. Cyndi loves going to the beach, on girl trips with her friends,
                         and is an avid deer hunter.</p>
                     <p>Cyndi started in the healthcare field as a CNA in the early 1980s and received her nursing
                         degree from Lamar University in 1997. She has primarily worked with the geriatric population
                         most of her career. She has worked in the settings of doctor office, LTC, and private duty
                         nursing, with the largest amount spent in the home health field of nearly 20 years. Cyndi has a
                         wealth of knowledge and experience that is a definite asset in our senior community.</p>
                     <p>It is very evident Cyndi loves the seniors. She never hesitates to stop and talk with residents
                         and loves to listen to their stories and the wisdom the elderly population can provide. She is
                         very caring, not only to the residents, but her coworkers and is a team player.</p>
                 </div>
             </article>
             <hr />
             <article> <img alt="Maegan" class="img-responsive col-sm-6" src="./images/Maegan_Brown.jpg" />
                 <div class="col-sm-6 text-column">
                     <h2>Maegan Brown, RN</h2>
                     <h3 class="pb20">Vice President of Operations</h3>
                     <p>Maegan Brown and her husband Tony live in Hallsville. She has two grown children, Slade and Lexie, and two granddaughters, Kinzley and Adley.  Maegan is a Registered Nurse bringing 18 years of expertise in the fields of Alzheimer’s/Dementia, Geriatric, Cardiac, and Critical Care. After working with many diverse groups, she has developed a passion for caring for our seniors and is dedicated to preserving their dignity while providing the exceptional care they deserve.</p>
                     <p>Maegan has worked with residents in Assisted Living and Memory Care for many years as Wellness Director as well as Executive Director and understands the importance of including their families in all aspects of the development and implementation of the plan of care for the resident. She makes it a priority to be available to staff, residents and their families to secure their trust that not only are their physical needs being continually met, but that their emotional needs are met as well.</p>
                     
                 </div>
             </article>
             <hr />
             <article> <img alt="Kevin" class="img-responsive col-sm-6" src="./images/Kevin_Hall.jpg" />
                 <div class="col-sm-6 text-column">
                     <h2>Kevin Hall</h2>
                     <h3 class="pb20">Life Enrichment Director</h3>
                     <p>Kevin Hall was born in Waxahachie, Texas in March of 1964. The first 15 years of my wonderful
                         life was spent in Lancaster, Texas. We then moved to El Paso, Texas. Which was a huge culture
                         shock to my reality. My family has been involved in drag racing and dirt track racing on many
                         levels for over 100 years.</p>
                     <p>I started my college career at UT Texas, and finished at UT El Paso UTEP, better known as the
                         Harvard on the Border, where I played football and basketball. After college I coached football
                         for over 20 years. While on the field at a game, I had a stroke, where I had to learn to walk,
                         talk, read and write again. I moved to Lufkin, Texas because of the reputation of the doctors
                         that were treating me. I then met Kandes Holbrook who taught me the love of being an Activity
                         Director. I then pursued my Activity Director Certification as well as my National
                         Certification for Activity Professionals. And then became a Certified Dementia Practitioner. I
                         am also Arthritis Certified and a Certified Dietary Food Manager. I truly have a passion for
                         working in the senior community, as they have such great stories to tell. I would like to thank
                         the Pinnacle Team and Resident for allowing me to be a part of their team. </p>
                 </div>
             </article>
             <div class="topline text-center"> <span><img alt="" src="./images/arrow.png" /></span> </div>
             <!--- closing topline --->



         </div>
     </div>         
 </section>

 <?php include "site/footer.php"; ?>