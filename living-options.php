   
<?php 
include "site/header.php"; 
$templateLead = "ViewContent"
?>
    
    <section class="section5">
    	<div class="container">
    		<div class="row">
    			<div class="col-sm-12">
				
    				<div class="topline text-center">
						<span><img src="./images/arrow-down.png" /></span>
						</div><!--- closing topline --->
						
    					<div class="row">
						<img style="display:block; margin-left:auto; margin-right:40px;" alt="pet-friendly-logo" src="./images/pet-friendly-crest.png" width="70" height="101"align="right">
						<h2 style="padding-bottom:45px;"><font style="color:black">Assisted Living Floor Plans </font></h2>
						
							<article class="col-sm-4">
							<div style="background-color:#f5f3f0" class="post-hold">
								<h3><font style="color:#1b234b"><strong>The Broadmoor</strong></font></h3>
                            	<h4>Studio Unit</h4>
								<h4>Approximately 430 Square feet</h4>
								<article class="col-sm-12">
                     <a href="./images/lufkin_al-studio_unit.jpg"><img class="img-responsive" src="./images/lufkin_al-studio_unit.jpg" /></a>
                    </article>
						<ul style="list-style: none;" align="left";>
                        <li> 
							<img src="./images/arrow-bulletpoint.png" /> Spacious Design</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Kitchenette</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Refrigerator/Microwave</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Closet Lighting</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Individually Controlled Thermostats</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Wheelchair Accessible 
							Walk-in Shower</li>
						</ul>
						</div>
							</article>
							
							
    						<article class="col-sm-4">
								<div style="background-color:#f5f3f0" class="post-hold">
								<h3><font style="color:#1b234b"><strong>The Pierre</strong></font></h3>
                            	<h4>One Bedroom</h4>
								<h4>Approximately 553 Square feet</h4>
								<article class="col-sm-12">
                     <a href="./images/Lufkin AL-1Br Unit.jpg"><img class="img-responsive" src="./images/Lufkin AL-1Br Unit.jpg" /></a>
                    </article>
					<ul style="list-style: none;" align="left";>
                        <li> <img src="./images/arrow-bulletpoint.png" /> Spacious Design</li>
						<li><img src="./images/arrow-bulletpoint.png" /> Kitchenette</li>
						<li><img src="./images/arrow-bulletpoint.png" /> Refrigerator/Microwave</li>
						<li><img src="./images/arrow-bulletpoint.png" /> Closet Lighting</li>
						<li><img src="./images/arrow-bulletpoint.png" /> Individually Controlled Thermostats</li>
						<li><img src="./images/arrow-bulletpoint.png" /> Wheelchair Accessible 
							Walk-in Shower</li>
					</ul>	
								</div>
                            </article><!--- closing post-hold --->
							
    						<article class="col-sm-4">
							<div style="background-color:#f5f3f0" class="post-hold">
								<h3><font style="color:#1b234b"><strong>The Fountainbleu</strong></font></h3>
                            	<h4>Two Bedroom</h4> 
								<h4>Approximately 834 Square Feet</h4>
								<article class="col-sm-12">
                     <a href="./images/Lufkin AL-2Br Unit.jpg"><img class="img-responsive" src="./images/Lufkin AL-2Br Unit.jpg" /></a>
                    </article>
                        <ul style="list-style: none;" align="left";>
                        <li> 
							<img src="./images/arrow-bulletpoint.png" /> Spacious Design</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Kitchenette</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Refrigerator/Microwave</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Closet Lighting</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Individually Controlled Thermostats</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Wheelchair Accessible 
							Walk-in Shower</li>
						</ul>
                            </div><!--- closing post-hold --->
							</article>
							<article class="col-sm-4">
							<div style="background-color:#f5f3f0" class="post-hold">
								<h3><font style="color:#1b234b"><strong>Assisted Living Modest Studio</strong></font></h3>
                            	<h4>Studio Unit</h4>
								<h4>Approximately 380 Square feet</h4>
								<article class="col-sm-12">
                     <a href="./images/lufkin_al-studio_unit.jpg"><img class="img-responsive" src="./images/lufkin_al-studio_unit.jpg" /></a>
                    </article>
						<ul style="list-style: none;" align="left";>
                        <li> 
							<img src="./images/arrow-bulletpoint.png" /> Modest design</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Individually controlled thermostats</li>
						<li>
							<img src="./images/arrow-bulletpoint.png" /> Weehlchair accesible walk-in shower</li>
						</ul>
							
    					</div> <!-- closing row -->
    					</article>
    			</div>
                
                <div class="clearfix"></div>
                
                <p>Filled with light and all the conveniences you would expect, our spacious, comfortable floor plans are merely the starting point for your unique vision. Deluxe standard finishes, including granite countertops, modern appliances, and specialty flooring. They are a blank canvas waiting to be enhanced by your own style. That is what turns a residence at Pinnacle Senior Living into your home.</p>
				<p class="pb40">*Actual room layout and dimensions may vary.</p>
               
                <div class="topline text-center">
						<span><img src="./images/arrow.png" /></span>
					</div><!--- closing topline --->
                <div class="clearfix"></div>
                
    		</div><!-- closing row -->
    	</div>
    </section>

<?php include "site/footer.php"; ?>
